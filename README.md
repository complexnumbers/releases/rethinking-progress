# Build files for "Rethinking Progress" album

Index file: https://complexnumbers.gitlab.io/releases/rethinking-progress/

## License
### Rethinking Progress (c) by Victor Argonov Project

Rethinking Progress is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
